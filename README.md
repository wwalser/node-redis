# Node.js and MySQL for Bitbucket Pipelines

This repository contains a Dockerfile as well as a simple example that shows how you can run your own Docker container with Node.js and Redis on Bitbucket Pipelines.

The Docker image is using node 7.5.0 and whatever version of Redis Ubuntu's package manager installs

## Quickstart

### Using the image with Bitbucket Pipelines

Just copy/paste the YML below in your bitbucket-pipelines.yml and adapt the script to your needs.

```yaml
# This is a sample build configuration for Javascript.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Dockerhub as your build environment.
image: wwalser/node-redis

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - redis-server --daemonize yes # You'll need to start redis-server as part of your pipeline
          - npm install
          - npm test
```

